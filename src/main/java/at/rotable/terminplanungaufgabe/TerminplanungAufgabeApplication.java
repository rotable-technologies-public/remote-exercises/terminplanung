package at.rotable.terminplanungaufgabe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TerminplanungAufgabeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TerminplanungAufgabeApplication.class, args);
    }

}
